# ZabbixCloseInfo
Zabbixの未クローズInfoイベントをすべてクローズするやつ

# Requirements
## テスト環境
- Python 3.6.8
- PyZabbix

- Zabbix 4.4

# Documentaion
## Getting Started
- ZabbixサーバのURLを指定する
    ```
    zapi = ZabbixAPI('http://zabbixserverURL/')
    ```
- Zabbix APIを利用するための認証情報を指定する
    ```
    zapi.login('username', 'password')
    ```
## Usage
```
$ python3 close-info.py
7 events closed.
```

# Customize
## イベント条件を変更したい場合
- 処理したいイベントの条件は`event.get()`のパラメータで指定する
    - [Zabbix Documentation 4.4 - event.get](https://www.zabbix.com/documentation/4.4/manual/api/reference/event/get)

## 処理内容を変更したい場合
- 処理内容は`event.acknowledge()`のactionパラメータで指定する
    - [Zabbix Documentation 4.4 - event.acknowledge](https://www.zabbix.com/documentation/4.4/manual/api/reference/event/acknowledge)

