from pyzabbix import ZabbixAPI

zapi = ZabbixAPI('http://zabbixserverURL/')
zapi.login('username', 'password')

result = 0

for h in zapi.event.get(severities=1, acknowledged=0):
    zapi.event.acknowledge(eventids=h['eventid'], action=7, message='closed by python script')
    result += 1

print(str(result) + ' events closed.')
